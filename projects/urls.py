from django.urls import path
from projects.views import (
    ListProjectListVIew,
    ProjectDetailsDetailView,
    ProjectCreateView,
)


urlpatterns = [
    path("", ListProjectListVIew.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailsDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
