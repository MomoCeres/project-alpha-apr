from django.shortcuts import redirect, render
from projects.models import Project
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ListProjectListVIew(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "list_projects"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailsDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project_detail"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        new_project = form.save()
        new_project.owner = self.request.user
        new_project.save()
        return redirect("show_project", pk=new_project.id)

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)
